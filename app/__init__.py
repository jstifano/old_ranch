from flask_api import FlaskAPI
from flask_sqlalchemy import SQLAlchemy
from instance.config import app_config

# Inicializando SQLAlchemy
db = SQLAlchemy()

def create_app(config_name):

    ###### Controladores #########
    from app.api.controllers.client import client
    from app.api.controllers.product import product
    from app.api.services.catogory_service import category_product
    
    app = FlaskAPI(__name__, instance_relative_config=True)
    app.config.from_object(app_config[config_name])
    app.config.from_pyfile('config.py')
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
    db.init_app(app)

    ############################################################# 
    # Sección donde se agregan los blueprint con sus servicios  #
    #############################################################
    
    app.register_blueprint(client, url_prefix='/client')
    app.register_blueprint(category_product, url_prefix='/category_product')
    app.register_blueprint(product, url_prefix='/product')
    
    return app