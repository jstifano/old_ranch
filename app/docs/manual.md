## Levantar servidor de Flask: 

* `flask run` 

## Comando para actualizar el requirements con las dependencias

* `pip freeze` 

# MIGRACIONES: 

## Comando para inicializar las migraciones

* `python manage.py db init` 

## Comando para ejecutar la migraciones de los modelos 

* `python manage.py db migrate` 

## Comando para aplicarle las migraciones a la base de datos

* `python manage.py db upgrade` 