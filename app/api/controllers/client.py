################################################################
# Blueprint para todos los servicios de Clientes en el sistema # 
# @author :: Javier Stifano <jstifano18@gmail.com>             #
################################################################

from flask import Blueprint
from flask import request, jsonify, abort
from app.api.models.models import Client

client = Blueprint('client', __name__)

#Metodo para registrar un nuevo cliente en el sistema
@client.route("/create", methods=['POST'])
def create_client():
    if (str(request.data['name']) == None 
    or str(request.data['lastname']) == None 
    or str(request.data['id_number']) == None 
    or str(request.data['name']) == "" 
    or str(request.data['lastname']) == "" 
    or str(request.data['id_number']) == ""
    or str(request.data['name']) == "{}"
    or str(request.data['lastname']) == "{}"
    or str(request.data['id_number']) == "{}"):
        print("Abortando consumo de servicio de creacion de cliente.")
        abort(400)            
    else:
        client = Client(
            name=str(request.data['name']),
            lastname=str(request.data['lastname']),
            id_number=str(request.data['id_number']),
            email=str(request.data['email']),
            address=str(request.data['address']),
            cellphone=str(request.data['cellphone'])    
        )
        client.save()   
        response = jsonify({
            'id': client.id,
            'name': client.name,
            'lastname': client.lastname,
            'id_number': client.id_number,
            'email': client.email,
            'address': client.address,
            'cellphone': client.cellphone,
            'status_code': 200
        })
        return response

#Metodo para eliminar un cliente del sistema
@client.route('/delete', methods=['POST'])
def delete_client():
    if request.data['id'] == None or request.data['id'] == "":
        print("Abortando consumo de servicio de borrado de cliente")
        abort(400)
    else:
        client = Client.query.filter_by(id=request.data['id']).first()
        client.status = False
        db.session.commit()

        response = jsonify({
            'id': client.id,
            'name': client.name,
            'lastname': client.lastname,
            'id_number': client.id_number,
            'email': client.email,
            'address': client.address,
            'cellphone': client.cellphone,
            'status': client.status,
            'status_code': 200
        })
        return response

#Metodo para activar un cliente del sistema que ya se encuentra inactivo
@client.route('/activate', methods=['POST'])
def activate_client():
    if request.data['id'] == None or request.data['id'] == "":
        print("Abortando consumo de servicio de activado de cliente")
        abort(400)
    else:
        client = Client.query.filter_by(id=request.data['id']).first()
        client.status = True
        db.session.commit()

        response = jsonify({
            'id': client.id,
            'name': client.name,
            'lastname': client.lastname,
            'id_number': client.id_number,
            'email': client.email,
            'address': client.address,
            'cellphone': client.cellphone,
            'status': client.status,
            'status_code': 200
        })
        return response

#Metodo para buscar un cliente por cédula o correo
@client.route('/find', methods=['POST'])
def find_client():
    for key in request.data:
        if key == 'email':
            client = Client.query.filter_by(email=request.data['email']).first()
    
            if(client == None):
                response = jsonify({
                    'message': 'El cliente no fue encontrado.',
                    'status_code': 200
                })
                return response

            response = jsonify({
                'id': client.id,
                'name': client.name,
                'lastname': client.lastname,
                'id_number': client.id_number,
                'email': client.email,
                'address': client.address,
                'cellphone': client.cellphone,
                'status': client.status,
                'status_code': 200
            })
            return response
        elif key == 'id_number':
            client = Client.query.filter_by(id_number=request.data['id_number']).first()
    
            if(client == None):
                response = jsonify({
                    'message': 'El cliente no fue encontrado.',
                    'status_code': 200
                })
                return response

            response = jsonify({
                'id': client.id,
                'name': client.name,
                'lastname': client.lastname,
                'id_number': client.id_number,
                'email': client.email,
                'address': client.address,
                'cellphone': client.cellphone,
                'status': client.status
            })
            return response              
        else:
            print("Abortando consumo de servicio para buscar un cliente.")
            abort(400)
    
@client.route('/update', methods=['POST'])
def update_client():
    if 'id_number' in request.data:
        client = Client.query.filter_by(id_number=request.data['id_number']).first()

        if(client == None):
            response = jsonify({
                'message': 'El cliente no fue encontrado.',
                'status_code': 200
            })

        if 'email' in request.data:
            client.email = request.data['email']
        if 'cellphone' in request.data:
            client.cellphone = request.data['cellphone']
        if 'address' in request.data:
            client.address = request.data['address']

        client.save()

        response = jsonify({
            'id': client.id,
            'name': client.name,
            'lastname': client.lastname,
            'id_number': client.id_number,
            'email': client.email,
            'address': client.address,
            'cellphone': client.cellphone,
            'status': client.status,
            'status_code': 200    
        })
        return response
    else:
        print('Abortando consumo de servicio para actualizar la información del cliente.')
        abort(400)