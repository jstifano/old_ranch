#################################################################
# Blueprint para todos los servicios de Productos en el sistema #  
# @author :: Javier Stifano <jstifano18@gmail.com>              #
#################################################################

from flask import Blueprint
from flask import request, jsonify, abort
from app.api.models.models import Product
from datetime import datetime, date # Libreria para poder manejar fechas

product = Blueprint('product', __name__)

def _get_date():
    return date.today()

def _get_time():
    return str(datetime.now().strftime("%I:%M:%S %p"))

#Metodo para registrar un producto en el sistema
@product.route('/create', methods =['POST'])
def create_product():
    if('code_product' not in request.data 
    or 'name' not in request.data 
    or 'cost' not in request.data
    or 'margin' not in request.data
    or 'has_iva' not in request.data
    or 'id_category' not in request.data):
        print("Abortando consumo de servicio de creacion de producto.")
        print("No se enviaron todos los parámetros necesitados")
        abort(400)

    if (str(request.data['code_product']) == None 
    or str(request.data['name']) == None 
    or float(request.data['cost']) == None
    or int(request.data['margin']) == None
    or str(request.data['has_iva']) == None
    or int(request.data['id_category']) == None
    or str(request.data['code_product']) == "" 
    or str(request.data['name']) == "" 
    or float(request.data['cost']) == ""
    or int(request.data['margin']) == ""
    or str(request.data['has_iva']) == ""
    or int(request.data['id_category']) == ""
    or str(request.data['code_product']) == "{}" 
    or str(request.data['name']) == "{}" 
    or float(request.data['cost']) == "{}"
    or int(request.data['margin']) == "{}"
    or str(request.data['has_iva']) == "{}"
    or int(request.data['id_category']) == "{}"):
        print("Abortando consumo de servicio de creacion de producto.")
        abort(400)

    # Convierto el IVA a 1 o 0 para que permita el valor booleando Postrges
    if( str(request.data['has_iva']) == "true" ):
        request.data['has_iva'] = 1
    else:
        request.data['has_iva'] = 0

    new_cost = float(request.data['cost']) # Seteo el costo para que tenga 2 decimales
    percent_margin = float( (float(request.data['margin']) * float(request.data['cost']) / 100.0 ) ) # Margen de ganancia del costo
    new_sales_cost = float(new_cost + percent_margin) # Costo de venta formateado con 2 decimales

    # Inicializo el producto que estoy creando
    new_product = Product(
        str(request.data['code_product']),
        str(request.data['name']),
        new_cost, # Costo bruto del producto
        int(request.data['margin']),
        new_sales_cost, #Costo de producto
        _get_date(),
        None,
        _get_time(),
        None,
        request.data['has_iva'],
        int(request.data['id_category'])  
    )   

    new_product.save()

    response = jsonify({
        'id': new_product.id,
        'code_product': new_product.code_product,
        'name': new_product.name,
        'cost': new_product.cost,
        'margin': new_product.margin,
        'sales_cost': new_product.sales_cost,
        'has_iva': new_product.has_iva,
        'status_code': 200
    })
    return response


    

    

