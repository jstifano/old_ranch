########################################################################
# Blueprint para darle autorizacion (Login) a los usuarios del sistema # 
# @author :: Javier Stifano <jstifano18@gmail.com>                     #
########################################################################

from flask import Blueprint
from flask import request, jsonify, abort
from app.api.models.models import Client

user = Blueprint('user', __name__)

# Método para loguearse en la aplicación
@user.route('/login', methods=['POST']):
def login():
     