######################################################################
# Servicio para inicializar todas las categorias en la base de datos #  
# @author :: Javier Stifano <jstifano18@gmail.com>                   #
######################################################################

from flask import Blueprint
from flask import request, jsonify, abort
from app.api.models.models import CategoryProduct
from datetime import datetime, date # Libreria para poder manejar fechas

category_product = Blueprint('category_product', __name__)

def _get_date():
    return date.today()

def _get_time():
    return str(datetime.now().strftime("%I:%M:%S %p"))

############### Arreglo con todas las categorías del sistema ###############
categories = [
    {
        'category_name': 'Bebidas no alcohólicas',
    },
    {
        'category_name': 'Vinos',
    },
    {
        'category_name': 'Pasta/Pizza/Rissoto',
    },
    {
        'category_name': 'Raciones',
    },
    {
        'category_name': 'Entradas-Entremeses',
    },
    {
        'category_name': 'Carnes a la parrilla',
    },
    {
        'category_name': 'Pescados y Mariscos',
    },
    {
        'category_name': 'Licores',
    },
    {
        'category_name': 'Ginebra y Vodkas',
    },
    {
        'category_name': 'Rones',
    },
    {
        'category_name': 'Postres',
    },
    {
        'category_name': 'Aperitivos',
    },
    {
        'category_name': 'Whisky',
    }
]
############### Arreglo con todas las categorías del sistema ###############

#Metodo para inicializar todas las categorías del sistema
@category_product.route('/init', methods =['GET'])
def init_products():
    for category in categories:
        new_category = CategoryProduct(str(category['category_name']), _get_date(), None, _get_time(), None)
        new_category.save()

    # The method created all the categories succesfully.
    return jsonify({
        'status_code': 200,
        'response': categories
    })     
