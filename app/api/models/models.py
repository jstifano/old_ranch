###########################################################################
# Modelos virtuales asociado a las tablas de la base de datos del sistema # 
# @author :: Javier Stifano <jstifano18@gmail.com>                        #
###########################################################################

from app import db
from sqlalchemy import ForeignKey
from sqlalchemy.orm import relationship
from datetime import datetime, date

def _get_date():
    return date.today().strftime("%Y-%m-%d")

def _get_time():
    return str(datetime.now().strftime("%I:%M:%S %p"))

# Tabla de factura (Modelo asociado a la BD)
class Bill(db.Model):
    
    #Nombre de la tabla de la base de datos real
    __tablename__ = 'bill'

    # Atributos de la tabla
    id = db.Column(db.Integer, primary_key=True, nullable=False) #Id de la factura(Requerido)
    code = db.Column(db.String(10), nullable=False) # Código de la factura (Requerido)
    created_at = db.Column(db.Date, nullable= False, default=_get_date()) # Fecha de creación de la factura (Requerido)
    time_created_at = db.Column(db.String(12), nullable=False, default=_get_time()) # Hora de creacion de la factura (Requerido)
    id_client = db.Column(db.Integer, ForeignKey('client.id'), nullable=False) # Llave foranea con la tabla "client" (Requerido)
    number_table = db.Column(db.Integer, nullable=False) # Número de mesa asociado a la factura (Requerido)
    is_open_table = db.Column(db.Boolean, nullable=False, default=True) # Booleano para saber si la mesa está abierta o no (Requerido)


# Tabla de cliente (Modelo asociado a la BD)
class Client(db.Model):

    #Nombre de la tabla de la base de datos real
    __tablename__ = 'client'

    # Atributos de la tabla
    id = db.Column(db.Integer, primary_key=True, nullable=False) #Id del cliente(Requerido)
    name = db.Column(db.String(30), nullable=False) # Nombre del cliente (Requerido)
    lastname = db.Column(db.String(30), nullable=False) # Apellido del cliente (Requerido)
    id_number = db.Column(db.String(12), nullable=False, unique=True) # Cédula o RIF del cliente (Requerido)
    email = db.Column(db.String(255), nullable=True ) # Correo electrónico del cliente (Opcional)
    address = db.Column(db.Text, nullable=True ) # Dirección de habitación del cliente (Opcional)
    cellphone = db.Column(db.String(20), nullable=True ) #Telefono celular del cliente (Opcional)
    status = db.Column(db.Boolean, nullable=False, default=True) #Estatus de activo o inactivo en el sistema (Requerido
    
    # Constructor del modelo Client
    def __init__(self, name, lastname, id_number, email=None, address=None, cellphone=None):
        self.name = name
        self.lastname = lastname
        self.id_number = id_number
        self.email = email
        self.address = address
        self.cellphone = cellphone
        self.status = True

    #Metodo para guardar el model Client
    def save(self):
        db.session.add(self)
        db.session.commit()

    #Metodo estático para obtener los clientes registrados
    @staticmethod
    def get_all():
        return Client.query.all()

    #Metodo para borrar un cliente en el modelo Client
    def delete(self):
        db.session.delete(self)
        db.session.commit()

    #Representacion del modelo Client en consultas
    def __repr__(self):
        return "<Clients: {}>".format(self.name)

# Tabla de producto (Modelo asociado a la BD)
class Product(db.Model):

    #Nombre de la tabla de la base de datos real
    __tablename__ = 'product'

    # Atributos de la tabla
    id = db.Column(db.Integer, primary_key=True, nullable=False) #Id del producto(Requerido)
    code_product = db.Column(db.String(10), nullable=False) # Código del producto (Requerido)
    name = db.Column(db.String(80), nullable=False) # Nombre del producto (Requerido)
    cost = db.Column(db.Float, nullable=False) # Costo del producto (Requerido)
    margin = db.Column(db.Integer, nullable=False ) # Margen de ganancia del producto (Requerido)
    sales_cost = db.Column(db.Float, nullable=False) # Costo de venta del producto (Requerido)
    created_at = db.Column(db.Date, nullable=False, default=_get_date()) # Fecha de creación del producto (Requerido)
    updated_at = db.Column(db.Date, onupdate=_get_date) # Fecha de actualización del producto (Requerido)
    time_created_at = db.Column(db.String(12), nullable=False, default=_get_time()) # Hora de creacion del producto (Requerido)
    time_updated_at = db.Column(db.String(12), onupdate=_get_time()) # Hora de actualizacion del producto (Requerido)
    has_iva = db.Column(db.Boolean, nullable=False) # Booleano para saber si el producto tiene IVA o está excento (Requerido)
    id_category = db.Column(db.Integer, ForeignKey('category_product.id'), nullable=False) # Llave foránea hacia la tabla de "category_product" (Requerido)

    # Constructor del modelo Product
    def __init__(self, code_product, name, cost, margin, sales_cost, created_at, updated_at, time_created_at, time_updated_at, has_iva, id_category):
        self.code_product = code_product
        self.name = name
        self.cost = cost
        self.margin = margin
        self.sales_cost = sales_cost
        self.created_at = created_at
        self.updated_at = updated_at
        self.time_created_at = time_created_at
        self.time_updated_at = time_updated_at
        self.has_iva = has_iva
        self.id_category = id_category
        
    # Metodo para guardar el modelo Product
    def save(self):     
        db.session.add(self)
        db.session.commit()


# Tabla de asociacion entre Bill y Product
class Bill_Join_Product(db.Model):
    
    #Nombre de la tabla de la base de datos real
    __tablename__ = 'bill_join_product'

    # Atributos de la tabla
    id_bill = db.Column(db.Integer, ForeignKey('bill.id'), nullable=False, primary_key=True) # Llave foranea con la tabla "bill" (Requerido)
    id_product = db.Column(db.Integer, ForeignKey('product.id'), nullable=False, primary_key=True) # Llave foranea con la tabla "product" (Requerido)
    quantity = db.Column(db.Integer, nullable=False) # Cantidad de productos para la descripcion de la factura (Requerido)
    created_at = db.Column(db.Date, nullable=False, default=_get_date()) # Fecha de creación del agregado del producto a la factura (Requerido)
    time_created_at = db.Column(db.String(12), nullable=False, default=_get_time()) # Hora de creacion del agregado del producto a la factura (Requerido)

# Tabla de categorías de productos
class CategoryProduct(db.Model):

    #Nombre de la tabla de la base de datos real
    __tablename__ = 'category_product'

    # Atributos de la tabla
    id = db.Column(db.Integer, primary_key=True, nullable=False) # Id de la tabla categoria de producto (Requerido)
    category_name = db.Column(db.String(80), nullable=False ) # Nombre o especificacion de la categoria (Requerido)
    created_at = db.Column(db.Date, nullable=False, default=_get_date()) # Fecha de creación del agregado de la categoría del producto (Requerido)
    updated_at = db.Column(db.Date, onupdate=_get_date()) # Fecha de actualización de la categoria de los productos
    time_created_at = db.Column(db.String(12), nullable=False, default=_get_time()) # Hora de creacion de las categorías (Requerido)
    time_updated_at = db.Column(db.String(12), onupdate=_get_time()) # Hora de actualizacion de las categorías (Requerido)
    
    # Constructor del modelo CategoryProduct
    def __init__(self, category_name, created_at, updated_at, time_created_at, time_updated_at):
        self.category_name = category_name
        self.created_at = created_at
        self.updated_at = updated_at
        self.time_created_at = time_created_at
        self.time_updated_at = time_updated_at

    # Metodo para guardar el modelo CategoryProduct
    def save(self):
        db.session.add(self)
        print("Se guardo la categoría ::: ", self.category_name)
        db.session.commit()

    # Metodo estático para obtener las categorias registradas
    @staticmethod
    def get_all():
        return CategoryProduct.query.all()

    # Metodo para borrar una categoria en el modelo CategoryProduct
    def delete(self):
        db.session.delete(self)
        db.session.commit()

    # Representacion del modelo CategoryProduct en consultas
    def __repr__(self):
        return "<Category product: {}>".format(self.category_name)

# Modelo para guardar los usuarios de la aplicacion
class User(db.Model):

    #Nombre de la tabla de la base de datos real
    __tablename__ = 'user'

    # Atributos de la tabla
    id = db.Column(db.Integer, primary_key=True, nullable=False) #Id del usuario del sistema(Requerido)
    username = db.Column(db.String(20), nullable=False) # Username del usuario del sistema (Requerido)
    password = db.Column(db.String(200), nullable= False) # Contraseña del usuario del sistema (Requerido)
    created_at = db.Column(db.Date, nullable=False, default=_get_date()) # Hora de creacion de la factura (Requerido)
    time_created_at = db.Column(db.String(12), nullable=False, default=_get_time())

