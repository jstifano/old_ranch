import os
from flask_script import Manager # Clase para manejar todo el conjunto de comandos
from flask_migrate import Migrate, MigrateCommand
from app import db, create_app
from app.api.models import models

app = create_app(config_name=os.getenv('APP_SETTINGS'))
migrate = Migrate(app, db)
manager = Manager(app)

manager.add_command('db', MigrateCommand)

if __name__ == '__main__':
    manager.run()