# Resumen para configuración del API #

* Abajo encontrarás todos los pasos necesarios para tener tu API levantada y corriendo correctamente.

### Descripción de la Aplicación: 

* Pequeña API RESTFUL donde se alojaran todos los servicios del sistema. 
* El API será desarrollada en el lenguaje de Python en su versión 3.7 y el framework Flask en su versión 1.0.2

### Instalación del API:

1. Descargar Python version 3.7 en su página oficial **https://www.python.org/**

2. Ir la Windows Powershell o CMD (Consola de comandos) y escribir:  ```python```

3. Luego de escribirlo, debería aparecer algo como esto:  `>>>`

4. Sí ves que aparace eso en la consola de comando, ya tienes instalado Python correctamente.  Typear `exit()` para salir.

6. Clonar el repositorio con el código del proyecto con el siguiente comando: `git clone https://jstifano@bitbucket.org/jstifano/billing_system.git`

7. Pararse en la carpeta ROOT del proyecto, en ese caso  **/old_ranch** 

8. Crear un ambiente virtual para nuestro proyecto, ejecutar el comando: `pip install virtualenv`

* Luego ejecutar el comando: `virtualenv venv` para tener ya disponible nuestro ambiente

* Para activar el ambiente virtual ejecutamos: `.\venv\Scripts\activate` y saldrá algo como: `(venv) PS C:\Users\jstif\Documents\Workspace\billing_system>`
* Para desactivar nuestro ambiente virtual solo ejecutamos `deactivate`

8. Dentro de la carpeta, ejecutar el comando  `pip install -r requeriments.txt`  esto se realiza para instalar todas las dependencias necesarias del proyecto

9. Ejecutar el comando: `flask run` para levantar el servidor.  Aparecerá en consola corriendo en la siguiente URL: **http://localhost:5000**